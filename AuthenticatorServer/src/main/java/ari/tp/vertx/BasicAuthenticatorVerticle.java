package ari.tp.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;

public class BasicAuthenticatorVerticle extends AbstractVerticle {
    @Override
    public void start() throws Exception {
        VertxOptions options = new VertxOptions();
        Vertx.clusteredVertx(options, res -> {
            if (res.succeeded()) {
                vertx = res.result();
                vertx.eventBus().consumer("authentication.requests", message -> {
                    System.out.println("Getting message");
                    message.reply(MainAuthenticatorVerticle.checkAuthentication((JsonObject) message.body()));
                });
            } else {
                System.out.println("Failed : " + res.cause());
            }
        });
    }
}
