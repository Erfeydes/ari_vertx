package ari.tp.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class HTTPAuthenticator extends AbstractVerticle {
    public void authenticate(RoutingContext routingContext){
        routingContext.request().bodyHandler(handler -> {
            JsonObject body = handler.toJsonObject();
            System.out.println(body);
            JsonObject response = MainAuthenticatorVerticle.checkAuthentication(body);

            boolean isSuccess = (boolean) response.getValue("success");

            routingContext.response()
                    .setStatusCode(isSuccess ? 200 : 400)
                    .putHeader("Content-Type", "application/json")
                    .end(String.valueOf(response));
        });
    }

    @Override
    public void start() throws Exception {
        Router router = Router.router(vertx);
        router.post("/auth").handler(this::authenticate);
        vertx.createHttpServer().requestHandler(router::accept).listen(6958);
    }
}
