package ari.tp.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class MainAuthenticatorVerticle extends AbstractVerticle {
    private static Properties userProperties;

    private static void initializeUsers() {
        System.out.println("Initializing...");

        userProperties = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        try {
            InputStream in = loader.getResourceAsStream("user.properties");
            userProperties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static JsonObject checkAuthentication(JsonObject payload){
        JsonObject returnMessage = new JsonObject();

        try {
            String login = payload.getString("login");
            String password = payload.getString("password");

            System.out.printf("Received :\n login : %s, password : %s\n", login, password);

            returnMessage.put("success", false);
            if (login == null && password == null) returnMessage.put("reason", "No login and password given.");
            else if (login == null) returnMessage.put("reason", "No login given.");
            else if (password == null) returnMessage.put("reason", "No password given.");

            if (login != null && userProperties.containsKey(login) && userProperties.getProperty(login).equals(password)) {
                returnMessage.put("success", true);
            }
        } catch (NullPointerException e){
            returnMessage.put("success", false);
            returnMessage.put("reason", e.getMessage());
        }

        return returnMessage;
    }

    @Override
    public void start() {
        this.initializeUsers();

        vertx.deployVerticle(new BasicAuthenticatorVerticle(), new DeploymentOptions().setInstances(1));
        vertx.deployVerticle(new MessengerVerticle());
        vertx.deployVerticle(HTTPAuthenticator.class.getCanonicalName(), new DeploymentOptions().setInstances(4));
    }
}
