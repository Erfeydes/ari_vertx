package ari.tp.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;

public class MessengerVerticle extends AbstractVerticle {
    static String login = null;
    static String password = null;

    @Override
    public void start() throws Exception {
        vertx.eventBus().send(
                "authentication.requests",
                new JsonObject()
                        .put("login", login)
                        .put("password", password),
                reply -> {
                    if (reply.succeeded()){
                        System.out.println("Reply : ");
                        System.out.println(reply.result().body());
                    } else {
                        System.out.println("Failed : " + reply.cause());
                    }
                });
    }

    public static void main(String... args) {
        if (args.length >= 1) login = args[0];
        if (args.length >= 2) password = args[1];

        VertxOptions options = new VertxOptions();
        Vertx.clusteredVertx(options, res -> {
            if (res.succeeded()) {
                Vertx vertx = res.result();
                vertx.deployVerticle(new MessengerVerticle());
            } else {
                System.out.println("Failed : " + res.cause());
            }
        });
    }
}
