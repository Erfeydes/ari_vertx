package ari.tp.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

public class MyMathVerticle extends AbstractVerticle {
    private void add(RoutingContext routingContext) {
        final String firstParam = routingContext.request().getParam("first");
        final String secondParam = routingContext.request().getParam("second");
        final int first = firstParam == null || firstParam.trim().isEmpty() ? 0 : Integer.parseInt(firstParam);
        final int second = secondParam == null || secondParam.trim().isEmpty() ? 0 : Integer.parseInt(secondParam);
        routingContext.request().response().end(String.valueOf(first + second));
    }

    private void multiply(RoutingContext routingContext) {
        final String firstParam = routingContext.request().getParam("first");
        final String secondParam = routingContext.request().getParam("second");
        final int first = firstParam == null || firstParam.trim().isEmpty() ? 1 : Integer.parseInt(firstParam);
        final int second = secondParam == null || secondParam.trim().isEmpty() ? 1 : Integer.parseInt(secondParam);
        routingContext.request().response().end(String.valueOf(first * second));
    }

    private void isEven(RoutingContext routingContext) {
        final String numberParam = routingContext.request().getParam("number");
        routingContext.request().response()
                .end(numberParam == null || numberParam.trim().isEmpty() ? "error" : (Integer.parseInt(numberParam) % 2 == 0 ? "true" : "false"));
    }

    @Override
    public void start() {
        Router router = Router.router(vertx);
        router.get("/api/v1/add").handler(this::add);
        router.get("/api/v1/multiply").handler(this::multiply);
        router.get("/api/v1/even/:number").handler(this::isEven);
        vertx.createHttpServer().requestHandler(router::accept).listen(8080);
    }
}
